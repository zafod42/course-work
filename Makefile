TARGET := shannon 
PREFIX := /usr/local/bin
SRC := main.cpp TextFrequencer.cpp
OBJ := $(SRC:.cpp=.o)

CPP := g++ -o 

.PHONY: all clean install uninstall

all: $(TARGET)

main.o: main.cpp
	$(CPP) main.o -c main.cpp

TextFrequencer.o: TextFrequencer.cpp
	$(CPP) TextFrequencer.o -c TextFrequencer.cpp

$(TARGET): $(OBJ)
	$(CPP) $(TARGET) $(OBJ)

clean:
	rm -rf $(TARGET) $(OBJ) 

install:
	install $(TARGET) $(PREFIX)

uninstall:
	rm -rf $(PREFIX)/$(TARGET)
