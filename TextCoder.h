#ifndef __TEXT_FREQ_H
#define __TEXT_FREQ_H

#include <iostream>
#include <fstream>
#include "BinaryTree.h"
#include "Pair.h"

class TextCoder
{
private:
	std::string filename;
	Pair* freqMap;
	size_t size;
	BinaryTree tree;

	void sortByFreqs();
	void calculateFreqs();

public:
	TextCoder(const std::string& filename);
	TextCoder(const TextCoder&) = delete;
	TextCoder(TextCoder&&) = delete;

	TextCoder& operator=(const TextCoder&) = delete;
	TextCoder& operator=(TextCoder&&) = delete;

	~TextCoder();
	
	void shannon();
	void showShannon(std::ostream&);
	double getCompressRatio();
	bool encode(const std::string& dest);
	bool decode(const std::string& source, const std::string& dest);

};

#endif // !__TEXT_FREQ_H
