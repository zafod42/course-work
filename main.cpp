#include "TextCoder.h"
#include <iostream>
#include <string>

void usage()
{
	std::cout << "./shannon <filename>\n";
}

std::string& makeEncode(std::string& tmp, const char* arg)
{
	tmp.append("encoded_");
	tmp.append(arg);
	tmp.append(".txt");
	return tmp;
}

std::string& makeDecode(std::string& tmp, const char* arg)
{
	tmp.append("decoded_");
	tmp.append(arg);
	tmp.append(".txt");
	return tmp;
}

std::string& makeCodeTableName(std::string& tmp, const char* arg)
{
	tmp.append("codetable_");
	tmp.append(arg);
	tmp.append(".txt");
	return tmp;
}
int main(int argc, char* argv[])
{
	if (argc != 2)
	{
		usage();
		exit(1);
	}
	std::string codeTableFilename;
	std::string enc, dec;
	TextCoder text(argv[1]);
	text.shannon();
	makeEncode(enc, argv[1]);
	text.encode(enc);
	makeDecode(dec, argv[1]);
	text.decode(enc, dec);
	makeCodeTableName(codeTableFilename, argv[1]);
	std::ofstream out(codeTableFilename);
	text.showShannon(out);
	std::cout << text.getCompressRatio() * 100 << "% compressed";
	return 0;
}
