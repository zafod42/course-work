#ifndef _BINARY_SEARCH_TREE_H
#define _BINARY_SEARCH_TREE_H

#include <iostream>
#include "Queue/fixedQueue.hpp"
#include "Pair.h"

enum
{
	ROOT = -1,
	LEFT = 0,
	RIGHT = 1
};

struct Node {
	std::string key_;
	Node* left_; 
	Node* right_; 
	Node* p_; 
	// ����������� ����
	Node(std::string key, Node* left = nullptr, Node* right = nullptr, Node* p = nullptr);
	~Node();
};

 
class BinaryTree
{
public:
	BinaryTree();
	BinaryTree(const BinaryTree& scr) = delete;
	BinaryTree(BinaryTree&& scr) noexcept;
	BinaryTree & operator= (const BinaryTree & src) = delete;
	BinaryTree & operator= (BinaryTree && src) noexcept;
	virtual ~BinaryTree();

	std::string getCurrentKey();
	
	int getCount() const;
	void go(int branch);
	std::string getCode(const char& key);
	void makeCodeTable(Pair* freqMap, size_t size);

private:
	
	Node* current;
	Node* root_; // ��������� �� �������� ����
	Node* searchNode(const char& key);
	int getCount(const Node* root) const;
	void makeShannonFano(int branch, Node* fullBranch, int start, int end, std::string alph, int* freqs);

	Node* getRoot();
	Node* insert(const std::string& key, bool branch, Node* current);


}; // ����� ������ BinaryTree

#endif // !_BINARY_SEARCH_TREE_H
