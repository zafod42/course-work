#include "BinaryTree.h"

Node::Node(std::string key, Node* left, Node* right, Node* p) :
	key_(key), left_(left), right_(right), p_(p)
{ }

Node::~Node()
{
	delete left_;
	delete right_;
}

BinaryTree::BinaryTree()
{
	root_ = new Node("", nullptr, nullptr, nullptr);
	current = root_;
}

BinaryTree::BinaryTree(BinaryTree&& src) noexcept
{
	if (this != &src)
	{
		this->root_ = src.root_;
		this->current = this->root_;
		src.root_ = nullptr;
		src.current = nullptr;
	}
}

BinaryTree& BinaryTree::operator=(BinaryTree&& src) noexcept
{
	if (this == &src)
	{
		return *this;
	}
	this->root_ = src.root_;
	this->current = this->root_;
	src.root_ = nullptr;
	src.current = nullptr;
	return *this;
}

BinaryTree::~BinaryTree()
{
	delete root_;
}

Node* BinaryTree::getRoot()
{
	return root_;
}

void BinaryTree::makeCodeTable(Pair* freqMap, size_t size)
{
	std::string alph;
	int* freqs = new int[size];
	for (int i = 0; i < size; ++i)
	{
		alph.push_back(freqMap[i].alph);
		freqs[i] = freqMap[i].freq;
	}
	makeShannonFano(-1, root_, 0, size - 1, alph, freqs);
	delete[] freqs;
}


Node* BinaryTree::insert(const std::string& key, bool branch, Node* current)
{
	if (branch == true)
	{
		current->right_ = new Node(key, nullptr, nullptr, current);
		return current->right_;
	}
	else
	{
		current->left_ = new Node(key, nullptr, nullptr, current);
		return current->left_;
	}
}


void BinaryTree::makeShannonFano(int branch, Node* fullBranch, int start, int end, std::string alph, int* freq)
{
	double dS = 0;
	int middle = 0, sumLeft = 0;

	if (branch != -1)
	{
		std::string ins;
		ins.append(alph.substr(start, end - start + 1));
		fullBranch = insert(ins, branch, fullBranch);
	}
	else
	{
		fullBranch = getRoot();
	}

	if (start == end)
	{
		std::string ins;
		ins.append(alph.substr(start, 1));
		insert(ins, branch, fullBranch);
		return;
	}

	for (int i = start; i < end; ++i)
	{
		dS += freq[i];
	}
	dS /= 2;

	sumLeft = 0;
	middle = start;
	for (int i = start; sumLeft + freq[i] < dS && i < end; ++i)
	{
		sumLeft += freq[i];
		++middle;
	}

	makeShannonFano(0, fullBranch, start, middle, alph, freq);
	makeShannonFano(1, fullBranch, middle + 1, end, alph, freq);
}

void BinaryTree::go(int branch)
{
	if (branch == ROOT)
	{
		current = root_;
	}
	if (branch == LEFT)
	{
		current = current->left_;
	}
	if (branch == RIGHT)
	{
		current = current->right_;
	}
}

std::string BinaryTree::getCode(const char& key)
{
	Node* node = searchNode(key);
	std::string code;
	Node* it = node->p_;
	while (it != nullptr)
	{
		if (it->left_ == node)
		{
			code.push_back('0');
		}
		else
		{
			code.push_back('1');
		}
		Node* tmp = it;
		it = it->p_;
		node = tmp;
	}
	std::string ret;
	for (int i = code.size() - 1; i >= 0; --i)
	{
		ret.push_back(code.at(i));
	}
	return ret;
}

Node* BinaryTree::searchNode(const char& key)
{
	
	Node* it = root_;
	std::string toFind;
	toFind.push_back(key);
	while (it->key_.size() != 1)
	{
		bool left = false;
		for (int i = 0; i < it->left_->key_.size(); ++i)
		{
			if (it->left_->key_[i] == key)
			{
				left = true;
			}
		}
		if (left)
		{
			it = it->left_;
		}
		else
		{
			it = it->right_;
		}
	}
	return it;
}

std::string BinaryTree::getCurrentKey()
{
	return current->key_;
}

int BinaryTree::getCount(const Node* node) const
{
	if (node == nullptr) return 1;
	return getCount(node->left_) + getCount(node->right_);
}


int BinaryTree::getCount() const
{
	return getCount(root_) - 1;
}