#include "TextCoder.h"

TextCoder::TextCoder(const std::string& f) : filename(f), freqMap(nullptr), size(0)
{
	std::fstream file;
	try 
	{
		file.open(filename);
		if (file.is_open() == false)
		{
			throw std::runtime_error("Error: no such file in directory");
		}
	}
	catch (std::runtime_error exc)
	{
		std::cerr << exc.what() << '\n';
		exit(1);
	}
	file.close();
}

TextCoder::~TextCoder()
{
	delete[] freqMap;
	size = 0;
}

bool addFrequence(char chr, Pair* arr, std::size_t size)
{
	for (int i = 0; i < size; ++i)
	{
		if (arr[i].alph == chr)
		{
			++arr[i].freq;
			return true;
		}
	}
	return false;
}

void TextCoder::calculateFreqs()
{
	char buff;
	std::ifstream file;
	file.open(filename);
	while ((buff = file.get()) != EOF)
	{
		if (addFrequence(buff, freqMap, size)) continue;

		Pair* tmp = new Pair[size + 1];
		for (int i = 0; i < size; ++i)
		{
			tmp[i] = freqMap[i];
		}
		tmp[size] = { buff, 1 };
		delete[] freqMap;
		freqMap = tmp;
		++size;
	}
	file.close();
}



void TextCoder::sortByFreqs()
{
	for (int i = 0; i < size; ++i)
	{
		for (int j = i + 1; j < size; ++j)
		{
			if (freqMap[i].freq < freqMap[j].freq)
			{
				Pair tmp = freqMap[i];
				freqMap[i] = freqMap[j];
				freqMap[j] = tmp;
			}
		}
	}
}

double TextCoder::getCompressRatio()
{
	size_t summ1 = 0, summ2 = 0;
	for (int i = 0; i < size; ++i)
	{
		std::string code = tree.getCode(freqMap[i].alph);
		summ1 += freqMap[i].freq * 8ull;
		summ2 += freqMap[i].freq * code.size();
	}
	return double(summ2) / double(summ1);
}

void TextCoder::shannon()
{
	calculateFreqs();
	sortByFreqs();
	tree.makeCodeTable(freqMap, size);
}

void TextCoder::showShannon(std::ostream& out)
{
	size_t summ1 = 0, summ2 = 0;
	for (int i = 0; i < size; ++i)
	{
		std::string ans = tree.getCode(freqMap[i].alph);
		out << freqMap[i].alph << " -> " << ans << '\n';
		summ1 += freqMap[i].freq * 8ull;
		summ2 += freqMap[i].freq * ans.size();
	}
	out << "total: " << (summ1 / 8.) / 1024 << " KiB  " << " vs.  " << (summ2 / 8.) / 1024 << " KiB" << '\n';
	std::cout << "total: " << (summ1 / 8.) / 1024 << " KiB  " << " vs.  " << (summ2 / 8.) / 1024 << " KiB" << '\n';
}

bool TextCoder::encode(const std::string& dest)
{
	std::fstream file(filename, std::ios::in);
	std::fstream out(dest, std::ios::out);
	char buff;
	while ((buff = file.get()) != EOF)
	{
		for (int i = 0; i < size; ++i)
		{
			if (freqMap[i].alph == buff)
			{
				out << tree.getCode(freqMap[i].alph);
			}
		}
	}
	return true;
}

bool TextCoder::decode(const std::string& source, const std::string& dest)
{
	std::ifstream in(source);
	std::ofstream out(dest);
	char buff;
	while ((buff = in.get()) != EOF)
	{
		if (buff == '0')
		{
			tree.go(LEFT);
		}
		else if (buff == '1')
		{
			tree.go(RIGHT);
		}
		std::string decodedKey = tree.getCurrentKey();
		if (decodedKey.size() == 1)
		{
			out << decodedKey;
			tree.go(ROOT);
		}
	}
	return true;
}